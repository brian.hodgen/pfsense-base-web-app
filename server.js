const express = require('express');
const bodyParser = require('body-parser');

const pfSenseRouter = require('./routes/mockpfSenseRouter.js');

const app = express();

app.use(bodyParser.json());
app.use(express.static('app'));

app.use('/api', pfSenseRouter);

app.listen(1337);
console.log('Listening on port 1337');