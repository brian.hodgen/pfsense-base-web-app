async function getRules()
{
    const response = await fetch('./api/rules');
    return await response.json();
}

async function enableRule(ruleID)
{
    await fetch(`./api/enable/${ruleID}`);
}

async function disableRule(ruleID)
{
    await fetch(`./api/disable/${ruleID}`);
}

new Vue({
    el: '#app',
    data() {
        return {
            isWorking: false,
            rules: []
        }
    },
    mounted: async function () {
        await this.refreshRules();
    },
    methods: {
        async refreshRules() {
            this.rules = await getRules();
            console.log(this.rules);
        },
        async clickedOn(ruleID)
        {
            this.isWorking = true;
            await enableRule(ruleID);
            await this.refreshRules();
            this.isWorking = false;
        },
        async clickedOff(ruleID)
        {
            this.isWorking = true;
            await disableRule(ruleID);
            await this.refreshRules();
            this.isWorking = false;
        }
    }
});