const express = require('express');

const router = express.Router();

const MOCK_DATA = [
    {
        tracker: "1",
        description: 'A:Living Room Devices',
        disabled: true
    },
    {
        tracker: "2",
        description: 'A:Emalee Devices',
        disabled: true
    },
    {
        tracker: "3",
        description: 'A:Jack Devices',
        disabled: true
    },
    {
        tracker: "4",
        description: 'Internal Rule',
        disabled: true
    }
];


router.get('/rules', (req, res) => {
    const filteredRules = MOCK_DATA.filter((rule) => {
        return rule.description.startsWith('A:');
    });

    const formatResults = filteredRules.map((rule) => {
        return {
            tracker: rule.tracker,
            disabled: rule.disabled,
            description: rule.description.replace('A:', '')
        }
    });

    res.json(formatResults);
    res.end();
});

router.get('/enable/:trackid', (req, res) => {
    const index = MOCK_DATA.findIndex((element) => {
        return element.tracker === req.params.trackid;
    });

    if (index > -1)
    {
        MOCK_DATA[index].disabled = false;
    }

    res.end();
});

router.get('/disable/:trackid', (req, res) => {
    const index = MOCK_DATA.findIndex((element) => {
        return element.tracker === req.params.trackid;
    });

    if (index > -1)
    {
        MOCK_DATA[index].disabled = true;
    }

    res.end();
});

module.exports = router;